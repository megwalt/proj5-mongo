"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""
import os
import flask
from flask import request
from pymongo import MongoClient

import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config


import logging

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.brevetsdb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")

    return flask.render_template('calc.html')


@app.route("/display")
def display():
    _items = db.brevetsdb.find()
    items = [item for item in _items]
    brevet_dist = {'kms': 0}

    items.sort(key=lambda item: item["miles"])

    if len(items) > 0:
        brevet_dist = items[0]
        app.logger.debug(brevet_dist)
        items = items[1:]
    
    return flask.render_template('display.html', items=items, bd=brevet_dist)
    


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    km = request.args.get('km', 999, type=float)
    brevets = request.args.get('brevets', type=int)
    start_time = request.args.get('start_time', type=str)
    arw_start = arrow.get(start_time).isoformat()
 
    open_time = acp_times.open_time(km, brevets, arw_start)
    close_time = acp_times.close_time(km, brevets, arw_start)
    result = {"open": open_time, "close": close_time}

    return flask.jsonify(result=result)


@app.route("/_submit", methods=["POST"])
def _submit():
    data = {
        'miles': request.form["miles"],
        'kms': request.form["kms"],
        'location': request.form["location"],
        'open': request.form["open"],
        'close': request.form["close"] 
    }
    db.brevetsdb.insert_one(data)

    return flask.jsonify(status=True)


@app.route("/_reset", methods=["POST"])
def _reset():
    global db
    db.brevetsdb.drop()
    db = client.brevetsdb
    
    return flask.jsonify(status=True)


@app.route("/_display", methods=["GET"])
def _display():
    _items = db.brevetsdb.find()
    items = [item for item in _items]

    status = (len(items) > 0)
    
    return flask.jsonify(status = status)


#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
